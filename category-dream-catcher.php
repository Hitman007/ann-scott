<?php
get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
<div class = "crg-old-title">Dream Catchers</div>
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); $link = get_permalink();?>
<div class = "crg-theme-background-color crg-color-bar"></div>
				     <div class="">
					 	<div style = "padding:10%;max-width:50%;min-width:50%;display:block;float:left;">
<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
<img src="<?php echo $url; ?>" />
						</div>
						<div style = "padding-top:10%;max-width:50%;min-width:50%;display:block;float:right;">
							<a href = "<?php echo ($link); ?>"><div class="as-underline-right crg-theme-color"><?php echo get_the_title(); ?></div></a>
							<div><?php echo get_the_content(); ?></div>
						</div>
					 </div>

 
			<?php endwhile; ?>
						

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
