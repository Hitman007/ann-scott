<?php
/*
Template Name: Two Columns	
*/
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class = "crg-half-page-right">
				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>
			</div><!-- END: -->
			<div class = "crg-half-page-left">
				<?php
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
					$thumb_url = $thumb_url_array[0];
				?>
				<img src = '<?php echo ($thumb_url); ?>' style = 'width:100%' />
			</div><!-- END crg half page -->
			<div class = "crg-half-page-left">
				<?php echo scott_comment_form(); ?>
			</div><!-- END: crg-half-page-left" -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>



