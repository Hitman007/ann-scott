<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package ann_scott
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='http://fonts.googleapis.com/css?family=Muli|Sacramento' rel='stylesheet' type='text/css'>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel='stylesheet' id='ann_scott-theme' href='<?php echo get_template_directory_uri(); ?>/style.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/custom-style.css' type='text/css' media='all' />
<?php wp_head(); ?>
<style>
#crg-right-half-of-site-title{}
#crg-left-half-of-site-title{color:#818282 !important;}
.crg-whole-site-title{font-size:250%;}
a, .crg-whole-site-title{text-decoration: none !important;}
#crg-new-site-title-space{float:right;text-align:right;font-size:250%;}
</style>
<script> 
jQuery(document).ready(function($){ 
/*  Use apropriate color code to integrate in html
#B845FF => Heliotrope
#00C3FF => Bright Turquoise
#41B8B0 => Puerto Rico
#A7CC3D => Atlantis
#F4CD15 => Ripe Lemon
#FF00B9 => Purple Pizzazz
#DD6112 => Christine
#AE2422 => Mexican Red
*/
	var menuids = [];
	var currentMenuItemID = '0'; 
	var colors = ["Heliotrope", "BrightTurquoise", "PuertoRico","Atlantis", "RipeLemon", "PurplePizzazz","Christine", "MexicanRed"];
	jQuery('.menu li').each(function() {
		menuids.push(jQuery(this).attr('id'));
		var clickableIcon = jQuery(this).html();
		clickableIcon = '<span>&nbsp;</span>' + clickableIcon;
		jQuery(this).html(clickableIcon);
		if (jQuery(this).hasClass("current-menu-item")){
			currentMenuItemID =  jQuery(this).attr('id');
			//alert (currentMenuItemID);
		}
	});
	var counter=0;
	jQuery.each(menuids, function(index, value) { 
  			jQuery('#'+value).addClass(colors[counter]);  
			if(index==7){counter=0;}else{counter++;}
	});
	
	
	jQuery.each(menuids, function(index, value) { 
			var allclass = jQuery('#'+value).attr('class');
			if( (allclass.split('current-menu-item')).length > 1){
				 console.log(window.location.href);
				var allhtml = jQuery('#'+value).html();
				var allhtml_arr = allhtml.split('href="');
				console.log(allhtml_arr);
				var allhtml_arr1 = allhtml_arr[1].split('">');
				console.log(allhtml_arr1);
				
				var allclass_arr = allclass.split(value);
				var current_colorcls = jQuery.trim(allclass_arr[1]);
				jQuery('ul#primary-menu').addClass(current_colorcls);
				
				var prop = jQuery('ul.'+current_colorcls).css('border-top');
				var prop_arr = prop.split('solid');
				var current_colorcode = jQuery.trim(prop_arr[1]);
				jQuery('.crg-theme-color').css('color',current_colorcode);
				jQuery('.crg-theme-background-color').css('background-color',current_colorcode);
			}
	});
	
	
	jQuery('#'+'menu-item-'+'currentMenuItemID').each(menuids, function(menuid){
		//alert(menuid);
	});
	jQuery('#primary-menu').css('border-top-color','white');
	
	var noSelected = 1;
	jQuery('.menu li').each(function() {
		menuids.push(jQuery(this).attr('id'));
		if (jQuery(this).hasClass("current-menu-item")){
			noSelected = 0;
		}
	});
	
	if (noSelected == 1){
		//alert('no selected');
		jQuery('.crg-theme-color').css('color','#d67d1f');
		jQuery('.crg-theme-background-color').css('background-color','#d67d1f');
	}
	
});
</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'ann_scott' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div id = "crg-new-site-title-space" class = "crg-theme-color">
			New Site Title
		</div>
		<div class="site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<span class = "crg-whole-site-title">
						<span id = "crg-left-half-of-site-title">Ann</span>
						<span id = "crg-right-half-of-site-title" class = "crg-theme-color" >Scott</span>
					</span> <!--END: .crg-whole-site-title -->
				</a>
		</div>
		<div id = "crg-header-bar" style = "width:100%;min-width:100%;display:block;clear:both;height:3px;" class = "crg-color-bar crg-theme-background-color"></div>
		<div id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php _e( 'Primary Menu', 'ann_scott' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</div><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
