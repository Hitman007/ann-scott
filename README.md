Notes:

In the content, you can use these classes: 

* .crg-theme-color this is the selected color 
* .as-underline-right
* .crg-style-font
* .crg-theme-color
* .crg-theme-background-color
* .crg-color-bar

To get a color bar:
<div class = "crg-color-bar crg-theme-background-color"></div>