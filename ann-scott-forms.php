<?php
function wholesale_app_form(){
	if(isset($_POST['viv_wholesale_sub']))
	{
		if($_POST['viv_contact_name'] != '' && $_POST['viv_business_name'] != '' && $_POST['viv_address'] != '' && $_POST['viv_city'] != '' && $_POST['viv_state'] != '' && $_POST['viv_zip_code'] != '' && $_POST['viv_phone'] != '' && $_POST['viv_fax'] != '' && $_POST['viv_email'] != '' && $_POST['viv_website'] != '' && $_POST['viv_resale_license'] != '')
		{
			$msg_body = '<p> Hi , Below are the details from <b> '.$_POST['viv_contact_name'].'</b> , which is sent from the WholeSale Application Form</p>';
			
			$msg_body .= '<p>Contact Name : '.$_POST['viv_contact_name'].'</p>';
			$msg_body .= '<p>Business Name : '.$_POST['viv_business_name'].'</p>';
			$msg_body .= '<p>Address : '.$_POST['viv_address'].'</p>';
			$msg_body .= '<p>City : '.$_POST['viv_city'].'</p>';
			$msg_body .= '<p>State : '.$_POST['viv_state'].'</p>';
			$msg_body .= '<p>Zip Code: '.$_POST['viv_zip_code'].'</p>';
			$msg_body .= '<p>Phone : '.$_POST['viv_phone'].'</p>';
			$msg_body .= '<p>Fax : '.$_POST['viv_fax'].'</p>';
			$msg_body .= '<p>Email : '.$_POST['viv_email'].'</p>';
			$msg_body .= '<p>Website : '.$_POST['viv_website'].'</p>';
			$msg_body .= '<p>Resale License : '.$_POST['viv_resale_license'].'</p>';
			$msg_body .= '<p>Request pdf Catalob : '.$_POST['viv_pdf_catalob'].'</p>';
			$msg_body .= '<p>Receive Design Releases : '.$_POST['viv_design_release'].'</p>';
			
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: Info<info@annscott.com>' . "\r\n";
			
			$sub = 'Details from WholeSale Application';
			
			/* $to = 'support@customrayguns.com';
			mail($to,$sub,$msg_body,$headers); */
			
			$to = 'jim@customrayguns.com';
			mail($to,$sub,$msg_body,$headers);
		}
		else
		{
			$error_msg = 'You have to fill all the fields.';
		}
	}
	
	
	$wsform_op = "<style>
		.viv_row {
			width: 100%;
			margin-top: 10px;
		}
		.viv_label {
			float: left;
			padding-right: 15px;
			width: 50%;
			text-align: right;
		}
	</style>
		<form action='' method='post'>
			<div class = 'viv_label crg-theme-color'>Wholesale Application :</div>
			<div ><span class = 'crg-theme-color' >*</span>all fields required</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>contact name :</div><div class = 'viv_input_div'><input type='text' name='viv_contact_name' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>business name :</div><div class = 'viv_input_div'><input type='text' name='viv_business_name' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>address :</div><div class = 'viv_input_div'><input type='text' name='viv_address' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>city :</div><div class = 'viv_input_div'><input type='text' name='viv_city' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>state :</div><div class = 'viv_input_div'><input type='text' name='viv_state' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>zip code :</div><div class = 'viv_input_div'><input type='text' name='viv_zip_code' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>phone :</div><div class = 'viv_input_div'><input type='text' name='viv_phone' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>fax :</div><div class = 'viv_input_div'><input type='text' name='viv_fax' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>email :</div><div class = 'viv_input_div'><input type='email' name='viv_email' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>website :</div><div class = 'viv_input_div'><input type='url' name='viv_website' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>resale license # :</div><div class = 'viv_input_div'><input type='text' name='viv_resale_license' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>request pdf catalob :</div><div class = 'viv_input_div'>
				<input type='radio' name='viv_pdf_catalob' value='Yes' checked class='crg-theme-background-color' />Yes
				<input type='radio' name='viv_pdf_catalob' value='No' class='crg-theme-background-color' />No
			</div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>receive design releases :</div><div class = 'viv_input_div'>
				<input type='radio' name='viv_design_release' value='Yes' checked  class='crg-theme-background-color' />Yes
				<input type='radio' name='viv_design_release' value='No' class='crg-theme-background-color' />No
			</div>
			</div>
			
			<div class = 'viv_row'>
			
			<div style='text-align: center;'>
				<input type='submit' class = 'crg-theme-color' name='viv_wholesale_sub' value='Request' />
			</div>
			</div>
			<div class = 'viv_row' style='color:red;'>".$error_msg."</div>
		</form>";
	return $wsform_op;
}

add_shortcode('WholeSale Application','wholesale_app_form');

function become_dreamcatcher_form(){
	if(isset($_POST['viv_become_dreamcatcher_sub']))
	{
		if($_POST['viv_name'] != '' && $_POST['viv_phone'] != '' && $_POST['viv_email'] != '' && $_POST['viv_use_award'] != '' && $_POST['viv_inspiration'] != '' && $_POST['viv_realize_dream'] != '' && $_POST['viv_disc_dream'] != '' && $_POST['viv_what_dream'] != '')
		{
			if($_POST['viv_become_dreamcatcher'] == 1)
				$msg_body = '<p> Hi , Below are the details from <b> '.$_POST['viv_name'].'</b> , who want to be a Dreamcatcher.</p>';
			else
				$msg_body = '<p> Hi , Details regarding <b>Some One</b> who want to become Dreamcatcher.</p>';
			
			$msg_body .= '<p>Name : '.$_POST['viv_name'].'</p>';
			$msg_body .= '<p>Phone : '.$_POST['viv_phone'].'</p>';
			$msg_body .= '<p>Email : '.$_POST['viv_email'].'</p>';
			$msg_body .= '<p>What is your dream ?: '.$_POST['viv_what_dream'].'</p>';
			$msg_body .= '<p>How did you discover your dream? : '.$_POST['viv_disc_dream'].'</p>';
			$msg_body .= '<p>What are you doing to realize your dream? : '.$_POST['viv_realize_dream'].'</p>';
			$msg_body .= '<p>Who or what is your inspiration?  : '.$_POST['viv_inspiration'].'</p>';
			$msg_body .= '<p>How will you use a dreamcatcher award? : '.$_POST['viv_use_award'].'</p>';
			
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: Info<info@annscott.com>' . "\r\n";
			
			$sub = 'Details from WholeSale Application';
			
			//$to = 'support@customrayguns.com';
			
			$to = 'jim@customrayguns.com';
			mail($to,$sub,$msg_body,$headers);
		}
		else
		{
			$error_msg = 'You have to fill all the fields.';
		}
	}
	
	
	$bdform_op = "<style>
	</style>
		<form action='' method='post'>
		
		<div class = 'viv_row'>
			<div class = 'viv_label crg-theme-color'>become a dreamcatcher :</div><div class = 'viv_input_div'>
				<input type='radio' name='viv_become_dreamcatcher' value='1' checked class='crg-theme-background-color' />yourself
				<input type='radio' name='viv_become_dreamcatcher' value='0' class='crg-theme-background-color' />someone else
			</div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>name :</div><div class = 'viv_input_div'><input type='text' name='viv_name' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>phone :</div><div class = 'viv_input_div'><input type='text' name='viv_phone' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>email :</div><div class = 'viv_input_div'><input type='email' name='viv_email' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>what is your dream? :</div><div class = 'viv_input_div'><input type='text' name='viv_what_dream' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>how did you discover your dream? :</div><div class = 'viv_input_div'><input type='text' name='viv_disc_dream' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>what are you doing to realize your dream? :</div><div class = 'viv_input_div'><input type='text' name='viv_realize_dream' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>who or what is your inspiration?  :</div><div class = 'viv_input_div'><input type='text' name='viv_inspiration' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div class = 'viv_label'>how will you use a dreamcatcher award? :</div><div class = 'viv_input_div'><input type='text' name='viv_use_award' required/></div>
			</div>
			
			<div class = 'viv_row'>
			<div style='text-align: center;'>
				<input type='submit' class = 'crg-theme-color' name='viv_become_dreamcatcher_sub' value='Apply' />
			</div>
			
			<div class = 'viv_row' style='color:red;'>".$error_msg."</div>
		</form>";
	return $bdform_op;
}

add_shortcode('Become Dreamcatcher','become_dreamcatcher_form');

function scott_comment_form(){

$formOutput = "
<form action = '/thank-you/' method = 'post'>
<table><tr>
<td style = 'text-align:right;font-size:150%' class = 'crg-theme-color'>Have a Question? :</td><td></td></tr>
<tr style = ''><td style = 'padding-top:15px; text-align:right;padding-right:10px;'>Name:</td><td><input type='text' style = 'padding-top:3px; width:100%;' name='asc_comment_name' /></td></tr>
<tr style = ''><td style = 'padding:10px;text-align:right;padding-right:10px;'>Email:</td><td><input style = 'padding-top:3px;width:100%;' type='text' name='asc_email' /></td></tr>
<tr style = ''><td style = 'padding:10px;text-align:right;padding-right:10px;'>Question:</td><td><input style = 'padding-top:3px;width:100%;' type='text' name='asc_question' /></td></tr>
<tr style = ''><td></td><td style = 'padding:10px;text-align:right;padding-right:10px;'><input type = 'submit' name = 'as_comment_submit' value = 'submit' label = 'Submit' style = ''padding-top:3px;' class = 'crg-theme-color'/></td>
</table>
</form>

";
return $formOutput;
}

add_shortcode('scott_comment_form','scott_comment_form');

if (isset($_POST['as_comment_submit'])){
	$msg_body .= '<p>NAME: '.$_POST['asc_comment_name'].'</p>';
	$msg_body .= '<p>EMAIL : '.$_POST['asc_email'].'</p>';
	$msg_body .= '<p>QUESTION : '.$_POST['asc_question'].'</p>';

	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Jim<jim@customrayguns.net>' . "\r\n";
	$sub = 'AnnScott Comment';
	//$to = 'support@customrayguns.com';
			
	$to = 'support@customrayguns.com';
	mail($to,$sub,$msg_body,$headers);
}
?>
