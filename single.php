<?php
/**
 * The template for displaying all single posts.
 *
 * @package ann_scott
 */
get_header(); ?>
<style>
.crg-full-width{width:81%;max-width:81%;margin:auto;}
</style>
	<div id="primary" class="content-area crg-full-width">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop.
			echo scott_comment_form(); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
