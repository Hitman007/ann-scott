function titleMove(){
	var title = jQuery(".crg-old-title").html();
	jQuery(".crg-old-title").hide();
	jQuery('#crg-new-site-title-space').text(title);
}

jQuery(document).ready(function(){
    jQuery('#crg_splash_page_center_div').fadeIn(3500); 
    jQuery('#reply-title').addClass('crg-theme-color');
    jQuery('.comment-form-author').addClass('crg-theme-color');
    jQuery('.comment-form-email').addClass('crg-theme-color');
    jQuery('.comment-form-comment').addClass('crg-theme-color');
	titleMove();
    
});
