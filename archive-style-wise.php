<?php get_header();?>
<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
<script>
//for some reason, jQuery doesn't seem to work on this page! Weird. Figure out why, or use /js/ann-scott.js
</script>
<style>
	.style-wise-question{}
	.style-wise-answer{font-size:250%;}
	.crg-archive-banner-image-div{max-width:20%;float:left;}
	.crg-archive-header-text-div{max-width:78%;float:right;}
	.crg-style-wise-header-area{margin-top:25px;}
</style>
<div class = "crg-old-title">Style Wise</div>
<div id = "crg-style-wise-header-area">
	<div class = "crg-archive-banner-image-div">
		<?php 
			//Stylewise banner image:
			$bannerUrl =  get_template_directory_uri() . "/assets/images/style-wise-banner.jpg";
			echo ("<img src = '$bannerUrl' class = 'crg-archive-banner-image' />");
		?>
	</div> <!--END: .crg-archive-banner-image -->
	<div class = "crg-archive-header-text-div crg-theme-color">
		<h1>
			Each Tuesday Ann Scott will test your StyleWise I.Q. — with a question about one of her favorite style icons. She’ll give you the answer to her stylish little secret on Thursday, so be sure to check back. favorite style icons.
		</h1>
	</div><!-- END: #crg-archive-header-text-div -->
</div><!-- END: #style-wise-header-area -->
<div class = "crg-color-bar"></div>
		<?php
			if ( have_posts() ) :
		?>

			<header class="page-header">

				<?php
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); $link = get_permalink();?>
<div class = "crg-theme-background-color crg-color-bar"></div>
				     <div class="">
					 	<div style = "padding:10%;max-width:50%;min-width:50%;display:block;float:left;">
<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
<img src="<?php echo $url; ?>" />
						</div>
						<div style = "padding-top:10%;max-width:50%;min-width:50%;display:block;float:right;">
							<div class="as-underline-right crg-theme-color"><?php echo get_the_title(); ?></div>
							<div><?php echo get_the_content(); ?></div>
						</div>
					 </div>

 
			<?php endwhile; ?>
						

		<?php endif; ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?> 
<?php get_footer(); ?>
