<?php
/**
 * The template for displaying "in-the-news" category posts.
 */

get_header(); ?>
<link rel="stylesheet" type="text/css" href="http://customrayguns.net/wp-content/themes/ann-scott/assets/fonts/font-awesome-4.3.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script>
$( document ).ready(function() {
    $('.up-down').click(function(){
		//$(this).closest('.post-box').children('.post-box-content').css('display','block');
		$(this).closest('.post-box').children('.post-box-content').slideToggle();
  	  $(this).find('i').toggleClass('fa-chevron-circle-down fa-chevron-circle-up')
	});
});
</script>
<style>
	.post-box {
	width:42%; 
	float:left; 
	border: 1px solid rgb(221, 220, 220);
	border-radius:7px;
	margin-left:4%;
	margin-right:4%;
	-webkit-box-shadow: 0px 9px 7px 0px rgba(214,214,214,0.72);
	-moz-box-shadow: 0px 9px 7px 0px rgba(214,214,214,0.72);
	box-shadow: 0px 9px 7px 0px rgba(214,214,214,0.72);
	margin-bottom:6%;
}
.up-down{
  position: relative;
  left: 88%;
  top: -23px;
  font-size: 20px;

}

.post-box-post-title {
  border-top: 2px rgb(116, 114, 114) dotted;
  padding-top: 2%;
 text-align:center;
   margin-bottom: -1%;
}
.post-box-content {
  margin-top: -10%;
  /* display: block; */
  padding: 15px;
}
.post-box-thumb img {
  border-top-left-radius: 7px;
  border-top-right-radius: 7px;
}
</style>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="entry-title crg-old-title crg-theme-color">In The News</h1>
			</header><!-- .page-header -->
			

			<?php /* Start the Loop */ ?>
			<div id="div-post-box">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php ?>
				     
				     <div class="post-box">
					     <div class="post-box-thumb"><?php echo get_the_post_thumbnail(); ?></div>
						<h3 class="post-box-post-title"><?php echo get_the_title(); ?></h3>
						<a class="up-down" href="javascript:void"><i class="fa fa-chevron-circle-down crg-theme-color"></i></a>
						<div class="post-box-content" style="display:none;">
						<?php echo the_excerpt(); ?>
						</div>
					 </div> 
			<?php endwhile; ?>
            </div>
			<?php the_posts_navigation(); ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
