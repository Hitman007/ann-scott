<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package ann_scott
 */
?>

	</div><!-- #content -->
<div class = "crg-color-bar crg-theme-background-color"></div>
	<footer id="colophon" class="site-footer" role="contentinfo">


		<div id = "crg-center-footer" style = "margin:auto;text-align:center;">
		57 haskell drive | bratenahl, ohio 44108 | 917.232.2350 | &copy; Ann Scott Design
		</div><!--end id = "crg-center-footer"-->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
