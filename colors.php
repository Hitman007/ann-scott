<?php

//This file is included in the header.php file of ann-scott

global $defaultColor;
global $CRGcolors;
$leftHalfOfSiteTitle = "black";
$defaultColor = "#d67d1f";
$CRGcolors[0] = "#B845FF";
$CRGcolors[1] = "#00C3FF";
$CRGcolors[2] = "#41B8B0";
$CRGcolors[3] = "#A7CC3D";
$CRGcolors[4] = "#F4CD15";
$CRGcolors[5] = "#DD6112";
$CRGcolors[6] = "#AE2422";
$maxColors = count($CRGcolors);

//Output the colors to jQuery:
echo ("<script>var maxColors = $maxColors;");
echo("var CRGcolors = [");
$x = 0;
while ($x < $maxColors){
    $color = $CRGcolors[$x];
    echo("'$color'");     
    if ($x < ($maxColors - 1)){echo (",");}
    $x++;
}
echo ("];var maxColors = $maxColors;</script>");
//END: //Output the colors to jQuery:

//Output the colors to CSS:
echo ("<style>");
$x = 0;
while ($x < $maxColors){
    $color = $CRGcolors[$x];
    echo(".ann-scott-color-");     
    echo ($x + 1);
    echo ("{color: $color;}");
    echo(".ann-scott-background-color-");     
    echo ($x + 1);
    echo ("{background-color: $color;}");
    $x++;
}
echo ("</style>");
//END: //Output the colors to CSS:

//Some CSS that is outputted during header.php:
echo ("

<style>

ul.menu {
  padding: 10px 50px 10px 50px;
  margin: 0px 50px 0px 50px;
  width: 100%;
}

ul.menu li{padding:25px;}

ul.menu li span {
  margin-right: 7px;
  box-shadow: 1px 2px 1px #CCCCCC;
  border-radius: 7px;
  border: 1px solid #C7C9C5;
}
ul.menu li span {
  width: 22px;
  height: 21px;
  display: inline-block; float:left;
}
html{font-family: 'Muli', sans-serif;}
#crg-right-half-of-site-title{}
#crg-left-half-of-site-title{color: $leftHalfOfSiteTitle !important;}
.crg-whole-site-title{font-size:250%;}
a, .crg-whole-site-title{text-decoration: none !important;}
.crg-theme-color{color: $defaultColor;}
.crg-theme-background-color{background-color: $defaultColor;}
.crg-style-font{font-family: 'Sacramento', cursive !important;}
.crg-color-bar{width:100%;min-width:100%;min-height:5px;clear:both;display:block;background-color: $defaultColor;}

</style>

"); //END echo ("

?>